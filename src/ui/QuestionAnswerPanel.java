/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * QuestionPanel.java
 *
 * Created on Nov 5, 2012, 2:28:02 PM
 */
package ui;

import JEE.test.DBConnection;
import com.bean.QuestionBean;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import org.scilab.forge.jlatexmath.*;


/**
 *
 * @author Administrator
 */
public class QuestionAnswerPanel extends javax.swing.JPanel {

    /** Creates new form QuestionPanel */
    String start,end;
    QuestionBean currentQuestion;
    QuestionBean hint;
    
    
    public QuestionAnswerPanel()
    {
        initComponents();  
    }
    
    public QuestionAnswerPanel(QuestionBean questionBean, int index ) {
        initComponents(); 
        
        start = "\\begin{array}{l}";
	end = "\\end{array}";
        
        lblHintm.setVisible(false);
        rdoOptionA.setEnabled(false);
        rdoOptionB.setEnabled(false);
        rdoOptionC.setEnabled(false);
        rdoOptionD.setEnabled(false);
        lblHint.setVisible(false);
        
        setQuestionOnPanel(questionBean, index);
//        rdoOptionA.setActionCommand("A");
//        rdoOptionB.setActionCommand("B");
//        rdoOptionC.setActionCommand("C");
//        rdoOptionD.setActionCommand("D");        
       
//        buttonGroup1.add(rdoOptionA);        
//        buttonGroup1.add(rdoOptionB);
//        buttonGroup1.add(rdoOptionC);
//        buttonGroup1.add(rdoOptionD);
    }
    
    public void showOptions()
    {
                lblA.setVisible(true);
                lblOptionA.setVisible(true);
                lblB.setVisible(true);
                lblOptionB.setVisible(true);
                lblC.setVisible(true);
                lblOptionC.setVisible(true);
                lblD.setVisible(true);
                lblOptionD.setVisible(true);
                lblOptionAsImage.setVisible(false);                
    }
    public void showHint()
    {
        
    }
    public void hideOptions()
    {
                lblA.setVisible(false);
                lblOptionA.setVisible(false);
                lblB.setVisible(false);
                lblOptionB.setVisible(false);
                lblC.setVisible(false);
                lblOptionC.setVisible(false);
                lblD.setVisible(false);
                lblOptionD.setVisible(false);
                lblOptionAsImage.setVisible(true);                
    }
    
    public void setLableText(JLabel l,String str)
    {
        try
        {
            if(str.equals(""))
            {
                
            }
            else
            {
                l.setText("");
                TeXFormula formula;
                TeXIcon icon;
                BufferedImage image;
                Graphics2D g2;                
                JLabel jl;
                str=start+str+end;
                formula = new TeXFormula(str);
                icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                icon.setInsets(new Insets(0,0,0,0));
                image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                g2 = image.createGraphics();
                g2.setColor(Color.white);
                g2.fillRect(0,0,icon.getIconWidth(),icon.getIconHeight());
                jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                l.setIcon(icon);   
            }
        }
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null,e.getMessage()); 
        }
    }
    
    public void showQuestionImage()
    {         
                lblQuestionAsImage.setVisible(true);                
    }
    public void hideQuestionImage()
    {                
                lblQuestionAsImage.setVisible(false);                
    }
    
    public void showHintImage()
    {         
                lblHintImage.setVisible(true);                
    }
    public void hideHintImage()
    {                
                lblHintImage.setVisible(false);                
    }
    
    public void loadImage(String path,JLabel lbl)
    {  
        System.out.println("Image path"+path);
        String[] ImagePath = path.split("/");
        System.out.println("Image path 1="+ImagePath[0]);
        System.out.println("Image path 2="+ImagePath[1]);
           
        String CurrentPatternName=new DBConnection().getCurrentPatternName();
        System.out.println("Image path 2 ="+CurrentPatternName+"/"+ImagePath[1]);
        String path1=CurrentPatternName+"/"+ImagePath[1];
        BufferedImage image;
        try{            
                File file=new File(path1);
                image=ImageIO.read(file);
                ImageIcon icon;
                float width=image.getWidth();
                float height=image.getHeight();
                //Determine how the image has to be scaled if it is large:
                Image thumb = image.getScaledInstance((int)width,(int)height, Image.SCALE_AREA_AVERAGING);
                icon=new ImageIcon(thumb);
                
                lbl.setIcon(icon);
                lbl.setText("");               
            }        
            catch(Exception e)
            {
                System.out.println(e);
            }
        }
    
    public void setQuestionOnPanel(QuestionBean question,int index)
    { 
             
            currentQuestion=question;
            hint=question;
            lblHintm.setVisible(false);
            if( question.getHint().equals("\\mbox{null}") ||question.getHint().equals("\\mbox{ }") ||question.getHint().equals("\\mbox{  }") ||question.getHint().equals("\\mbox{   }") ||question.getHint().equals("\\mbox{\\mbox{}}") ||question.getHint().equals("\\mbox{") ||question.getHint().equals("\\mbox{    }") ||question.getHint().equals("") ||question.getHint() == null||question.getHint().equals("\\mbox{}"))
                {
                    lblHintm.setVisible(false);
                    lblHint.setVisible(false);
                }
                else
                {
                    lblHint.setVisible(true);
                    lblHintm.setVisible(true);
                    setLableText(lblHintm,question.getHint());
                }
           
            setLableText(lblQuestion,question.getQuestion());
            if(!question.isIsQuestionAsImage())
            {
                hideQuestionImage();
            }
            else
            {
                showQuestionImage();                
                loadImage(question.getQuestionImagePath(), lblQuestionAsImage);                
            }
            if(!question.isIsHintAsImage())
            {
                hideHintImage();
            }
            else
            {
                showHintImage();                
                loadImage(question.getHintImagePath(), lblHintImage);                
            }
            if(!question.isIsOptionAsImage())
            {
                showOptions();                
                setLableText(lblOptionA,question.getOptionA());            
                setLableText(lblOptionB,question.getOptionB());
                setLableText(lblOptionC,question.getOptionC());
                setLableText(lblOptionD,question.getOptionD());
            }
            else
            {
                hideOptions();   
                loadImage(question.getOptionImagePath(), lblOptionAsImage);
            }            
            
            String userAnswer=question.getUserAnswer();
            if(question.getUserAnswer().equals("UnAttempted"))// userAnswer.equals("UnAttempted")
            {    
                System.out.println("******UnAttempted");        
                lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/unAttempted.png")));
                lblCorrectAnswer.setText(currentQuestion.getAnswer());
            }
            else
            {
                       if(userAnswer.equals("A")) 
                        {
                            rdoOptionA.setSelected(true);
                        }
                        else if(userAnswer.equals("B"))
                        {
                            rdoOptionB.setSelected(true);
                        }
                        else if(userAnswer.equals("C"))
                        {
                            rdoOptionC.setSelected(true);
                        }
                        if(userAnswer.equals("D"))
                        {
                            rdoOptionD.setSelected(true);
                        }     
                
                if(question.getUserAnswer().equals(question.getAnswer()))  //  userAnswer.equals(currentQuestion.getAnswer())
                {
                       
                     
                  lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/correct.png")));
//                  lblCorrect.setVisible(false);
                  lblCorrectAnswer.setText(currentQuestion.getAnswer());
//                  lblCorrectAnswer.setVisible(false);
                    System.out.println("******Correct Image");
                    
                }
                else
                { 
                    System.out.println("******InCorrect Image");
                    lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wrong.png")));
                    lblCorrectAnswer.setText(currentQuestion.getAnswer());
                }
            }            
            lblQuestionNo.setText("Q. "+index);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        lblOptionAsImage = new javax.swing.JLabel();
        rdoOptionA = new javax.swing.JRadioButton();
        lblOptionA = new javax.swing.JLabel();
        rdoOptionC = new javax.swing.JRadioButton();
        lblOptionB = new javax.swing.JLabel();
        rdoOptionB = new javax.swing.JRadioButton();
        lblB = new javax.swing.JLabel();
        lblD = new javax.swing.JLabel();
        lblOptionC = new javax.swing.JLabel();
        lblC = new javax.swing.JLabel();
        lblQuestionNo5 = new javax.swing.JLabel();
        lblQuestion = new javax.swing.JLabel();
        rdoOptionD = new javax.swing.JRadioButton();
        lblQuestionNo = new javax.swing.JLabel();
        lblA = new javax.swing.JLabel();
        lblQuestionNo6 = new javax.swing.JLabel();
        lblOptionD = new javax.swing.JLabel();
        lblQuestionAsImage = new javax.swing.JLabel();
        lblHint = new javax.swing.JLabel();
        lblRightWrongSymbol = new javax.swing.JLabel();
        lblCorrect = new javax.swing.JLabel();
        lblCorrectAnswer = new javax.swing.JLabel();
        lblHintm = new javax.swing.JLabel();
        lblHintImage = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setAutoscrolls(true);
        setPreferredSize(new java.awt.Dimension(765, 500));

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setName("jPanel1"); // NOI18N

        lblOptionAsImage.setText("Option Image");
        lblOptionAsImage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionAsImage.setName("lblOptionAsImage"); // NOI18N

        rdoOptionA.setText("A");
        rdoOptionA.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionA.setName("rdoOptionA"); // NOI18N
        rdoOptionA.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionAItemStateChanged(evt);
            }
        });

        lblOptionA.setText("jLabel7");
        lblOptionA.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionA.setName("lblOptionA"); // NOI18N

        rdoOptionC.setText("C");
        rdoOptionC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionC.setName("rdoOptionC"); // NOI18N
        rdoOptionC.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionCItemStateChanged(evt);
            }
        });

        lblOptionB.setText("jLabel7");
        lblOptionB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionB.setName("lblOptionB"); // NOI18N

        rdoOptionB.setText("B");
        rdoOptionB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionB.setName("rdoOptionB"); // NOI18N
        rdoOptionB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionBItemStateChanged(evt);
            }
        });

        lblB.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblB.setText("B :");
        lblB.setName("lblB"); // NOI18N

        lblD.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblD.setText("D :");
        lblD.setName("lblD"); // NOI18N

        lblOptionC.setText("jLabel7");
        lblOptionC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionC.setName("lblOptionC"); // NOI18N

        lblC.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblC.setText("C :");
        lblC.setName("lblC"); // NOI18N

        lblQuestionNo5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo5.setText("User Answer:");
        lblQuestionNo5.setName("lblQuestionNo5"); // NOI18N

        lblQuestion.setText("Question"); // NOI18N
        lblQuestion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQuestion.setName("lblQuestion"); // NOI18N

        rdoOptionD.setText("D");
        rdoOptionD.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionD.setName("rdoOptionD"); // NOI18N
        rdoOptionD.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionDItemStateChanged(evt);
            }
        });

        lblQuestionNo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo.setText("Q. 1");
        lblQuestionNo.setName("lblQuestionNo"); // NOI18N

        lblA.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblA.setText("A :");
        lblA.setName("lblA"); // NOI18N

        lblQuestionNo6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo6.setText("Options :");
        lblQuestionNo6.setName("lblQuestionNo6"); // NOI18N

        lblOptionD.setText("jLabel7");
        lblOptionD.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionD.setName("lblOptionD"); // NOI18N

        lblQuestionAsImage.setText("Question Image");
        lblQuestionAsImage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQuestionAsImage.setName("lblQuestionAsImage"); // NOI18N
        lblQuestionAsImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQuestionAsImageMouseClicked(evt);
            }
        });

        lblHint.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblHint.setText("Hint:");
        lblHint.setName("lblHint"); // NOI18N

        lblRightWrongSymbol.setBackground(new java.awt.Color(255, 255, 255));
        lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/correct.png"))); // NOI18N
        lblRightWrongSymbol.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblRightWrongSymbol.setName("lblRightWrongSymbol"); // NOI18N

        lblCorrect.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblCorrect.setText("Correct Answer:");
        lblCorrect.setName("lblCorrect"); // NOI18N

        lblCorrectAnswer.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblCorrectAnswer.setText("A");
        lblCorrectAnswer.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblCorrectAnswer.setName("lblCorrectAnswer"); // NOI18N

        lblHintm.setForeground(new java.awt.Color(255, 0, 0));
        lblHintm.setText("Hint");
        lblHintm.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblHintm.setName("lblHintm"); // NOI18N

        lblHintImage.setText("HintImage");
        lblHintImage.setName("lblHintImage"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCorrect)
                            .addComponent(lblHint))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblHintm)
                                .addContainerGap(534, Short.MAX_VALUE))
                            .addComponent(lblCorrectAnswer, javax.swing.GroupLayout.Alignment.LEADING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblQuestionNo)
                                .addGap(28, 28, 28)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblQuestionAsImage)
                                    .addComponent(lblQuestion)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblQuestionNo6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblB)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblOptionB))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblA)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblOptionA))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblC)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblOptionC))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblD)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblOptionD))
                                    .addComponent(lblOptionAsImage)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblQuestionNo5, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(rdoOptionA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoOptionB)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoOptionC)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoOptionD)
                                .addGap(18, 18, 18)
                                .addComponent(lblRightWrongSymbol)))
                        .addContainerGap(405, Short.MAX_VALUE))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(179, 179, 179)
                .addComponent(lblHintImage)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo)
                    .addComponent(lblQuestion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblQuestionAsImage)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo6)
                    .addComponent(lblA)
                    .addComponent(lblOptionA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB)
                    .addComponent(lblOptionB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblC)
                    .addComponent(lblOptionC))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblD)
                    .addComponent(lblOptionD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblOptionAsImage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblRightWrongSymbol)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblQuestionNo5)
                        .addComponent(rdoOptionA)
                        .addComponent(rdoOptionB)
                        .addComponent(rdoOptionC)
                        .addComponent(rdoOptionD)))
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCorrect)
                    .addComponent(lblCorrectAnswer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblHintm)
                    .addComponent(lblHint))
                .addGap(18, 18, 18)
                .addComponent(lblHintImage)
                .addContainerGap(110, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

private void rdoOptionBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionBItemStateChanged
   
}//GEN-LAST:event_rdoOptionBItemStateChanged

private void rdoOptionDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionDItemStateChanged
    
}//GEN-LAST:event_rdoOptionDItemStateChanged

private void rdoOptionAItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionAItemStateChanged
    
}//GEN-LAST:event_rdoOptionAItemStateChanged

private void rdoOptionCItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionCItemStateChanged
    
}//GEN-LAST:event_rdoOptionCItemStateChanged

    private void lblQuestionAsImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQuestionAsImageMouseClicked
        //JOptionPane.showMessageDialog(null, "Hello");
        
    }//GEN-LAST:event_lblQuestionAsImageMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblA;
    private javax.swing.JLabel lblB;
    private javax.swing.JLabel lblC;
    private javax.swing.JLabel lblCorrect;
    private javax.swing.JLabel lblCorrectAnswer;
    private javax.swing.JLabel lblD;
    private javax.swing.JLabel lblHint;
    private javax.swing.JLabel lblHintImage;
    private javax.swing.JLabel lblHintm;
    private javax.swing.JLabel lblOptionA;
    private javax.swing.JLabel lblOptionAsImage;
    private javax.swing.JLabel lblOptionB;
    private javax.swing.JLabel lblOptionC;
    private javax.swing.JLabel lblOptionD;
    private javax.swing.JLabel lblQuestion;
    private javax.swing.JLabel lblQuestionAsImage;
    private javax.swing.JLabel lblQuestionNo;
    private javax.swing.JLabel lblQuestionNo5;
    private javax.swing.JLabel lblQuestionNo6;
    private javax.swing.JLabel lblRightWrongSymbol;
    private javax.swing.JRadioButton rdoOptionA;
    private javax.swing.JRadioButton rdoOptionB;
    private javax.swing.JRadioButton rdoOptionC;
    private javax.swing.JRadioButton rdoOptionD;
    // End of variables declaration//GEN-END:variables
}