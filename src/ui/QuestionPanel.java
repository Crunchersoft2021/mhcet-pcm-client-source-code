/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * QuestionPanel.java
 *
 * Created on Nov 5, 2012, 2:28:02 PM
 */
package ui;

import JEE.test.DBConnection;
import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import org.scilab.forge.jlatexmath.*;


/**
 *
 * @author Administrator
 */
public class QuestionPanel extends javax.swing.JPanel {

    /** Creates new form QuestionPanel */
    String start, end;
    QuestionBean currentQuestion;
    String qImagePath = "", oImagePath = "";
    private ArrayList<ImageRatioBean> imageRatioList;
    public QuestionPanel() {
        initComponents();
        setOpaque(false);
        start = "\\begin{array}{l}";
        end = "\\end{array}";
        rdoOptionA.setActionCommand("A");
        rdoOptionB.setActionCommand("B");
        rdoOptionC.setActionCommand("C");
        rdoOptionD.setActionCommand("D");
        buttonGroup1.add(rdoOptionA);
        buttonGroup1.add(rdoOptionB);
        buttonGroup1.add(rdoOptionC);
        buttonGroup1.add(rdoOptionD);
//        setIconImage(new ImageIcon(getClass().getResource("/ui/images/c.gif")).getImage());
    }

    public String getUserAnswer() {
        ButtonModel buttonModel = buttonGroup1.getSelection();
        if (buttonModel != null) {
            String userAnswer = buttonModel.getActionCommand();
            System.out.println("***userAnswer="+userAnswer);
            if (!userAnswer.equals("") || userAnswer != null) {
                currentQuestion.setUserAnswer(userAnswer);
                return userAnswer;
            } else {
                return null;
            }
        }
        return null;
    }

    public void showOptions() {
        lblA.setVisible(true);
        lblOptionA.setVisible(true);
        lblB.setVisible(true);
        lblOptionB.setVisible(true);
        lblC.setVisible(true);
        lblOptionC.setVisible(true);
        lblD.setVisible(true);
        lblOptionD.setVisible(true);
        lblOptionAsImage.setVisible(false);
    }

    public void hideOptions() {
        lblA.setVisible(false);
        lblOptionA.setVisible(false);
        lblB.setVisible(false);
        lblOptionB.setVisible(false);
        lblC.setVisible(false);
        lblOptionC.setVisible(false);
        lblD.setVisible(false);
        lblOptionD.setVisible(false);
        lblOptionAsImage.setVisible(true);
    }

    public void setLableText(JLabel l, String str) {
        try {
            if (str.equals("")) {
            } else {
                l.setText("");
                TeXFormula formula;
                TeXIcon icon;
                BufferedImage image;
                Graphics2D g2;
                JLabel jl;
                str = start + str + end;
                formula = new TeXFormula(str);
                icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                icon.setInsets(new Insets(0, 0, 0, 0));
                image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                g2 = image.createGraphics();
                g2.setColor(Color.white);
                g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
                jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                l.setIcon(icon);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null,e.getMessage()); 
        }
    }

    public void showQuestionImage() {
        lblQuestionAsImage.setVisible(true);
    }

    public void hideQuestionImage() {
        lblQuestionAsImage.setVisible(false);
    }

//    public void loadImage(String path, JLabel lbl) {
//        BufferedImage image;
//        try {
//            File file = new File(path);
//            image = ImageIO.read(file);
//            ImageIcon icon;
//            float width = image.getWidth();
//            float height = image.getHeight();
////                width=(float) (width*0.3);
////                height=(float) (height*0.3);
//            //Determine how the image has to be scaled if it is large:
//            Image thumb = image.getScaledInstance((int) width, (int) height, Image.SCALE_AREA_AVERAGING);
//            icon = new ImageIcon(thumb);
//
//            lbl.setIcon(icon);
//            lbl.setText("");
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//    }
    
    public void loadImage(String path, JLabel lbl) {
        System.out.println("Image path"+path);
        String[] ImagePath = path.split("/");
        System.out.println("Image path 1="+ImagePath[0]);
        System.out.println("Image path 2="+ImagePath[1]);
           
        String CurrentPatternName=new DBConnection().getCurrentPatternName();
        System.out.println("Image path 2 ="+CurrentPatternName+"/"+ImagePath[1]);
        String path1=CurrentPatternName+"/"+ImagePath[1];
        
        BufferedImage image;
        try {
            File file = new File(path1);
            image = ImageIO.read(file);
            ImageIcon icon;
            float width = image.getWidth();
            float height = image.getHeight();
            float val = (float) 1.0;
            if(imageRatioList != null) {
                for(ImageRatioBean imageRatioBean : imageRatioList) {
                    if(imageRatioBean.getImageName().equalsIgnoreCase(path1)) {
                        val = (float) imageRatioBean.getViewDimention();
                        break;
                    }   
                }
            }
//            val = new Quest.chapterwise.QuestFirstPageSettingC().getImageDim(path);
            width = (float) (width * val);
            height = (float) (height * val);
 
//                width=(float) (width*0.3);
//                height=(float) (height*0.3);
            //Determine how the image has to be scaled if it is large:
            Image thumb = image.getScaledInstance((int) width, (int) height, Image.SCALE_AREA_AVERAGING);
            icon = new ImageIcon(thumb);

            lbl.setIcon(icon);
            lbl.setText("");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public void setQuestionOnPanel1(QuestionBean question, int index,ArrayList<ImageRatioBean> imageRatioList) {
//        currentQuestion = question;
        this.imageRatioList = imageRatioList;
        setLableText(lblQuestion, question.getQuestion());
        qImagePath = currentQuestion.getQuestionImagePath();
        oImagePath = currentQuestion.getOptionImagePath();
        if (!question.isIsQuestionAsImage()) {
            hideQuestionImage();
        } else {
//            ImageIcon image = new EmployeeShow().RetriveImage(question.getQuestionImagePath());
            showQuestionImage();
//            lblQuestionAsImage.setIcon(image);
//            lblQuestionAsImage.setText("");
            loadImage(question.getQuestionImagePath(), lblQuestionAsImage);                
        }
        if (!question.isIsOptionAsImage()) {
            showOptions();
            setLableText(lblOptionA, question.getOptionA());
            setLableText(lblOptionB, question.getOptionB());
            setLableText(lblOptionC, question.getOptionC());
            setLableText(lblOptionD, question.getOptionD());
        } else {
            hideOptions();
            loadImage(question.getOptionImagePath(), lblOptionAsImage);
//            ImageIcon image = new EmployeeShow().RetriveImage(question.getOptionImagePath());
//            lblOptionAsImage.setIcon(image);
            lblOptionAsImage.setText("");
        }

        String userAnswer = question.getUserAnswer();
        if (userAnswer.equals("UnAttempted")) {
            unlockSelection();
        } else {
            if (userAnswer.equals("A")) {
                rdoOptionA.setSelected(true);
            } else if (userAnswer.equals("B")) {
                rdoOptionB.setSelected(true);
            } else if (userAnswer.equals("C")) {
                rdoOptionC.setSelected(true);
            }
            if (userAnswer.equals("D")) {
                rdoOptionD.setSelected(true);
            }
//            lockSelection();
        }
        lblQuestionNo.setText("Q. " + index);
    }

    public void setQuestionOnPanel(QuestionBean question, int index) {
        currentQuestion = question;
        setLableText(lblQuestion, question.getQuestion());
        qImagePath = currentQuestion.getQuestionImagePath();
        oImagePath = currentQuestion.getOptionImagePath();
        if (!question.isIsQuestionAsImage()) {
            hideQuestionImage();
        } else {
//            ImageIcon image = new EmployeeShow().RetriveImage(question.getQuestionImagePath());
            showQuestionImage();
//            lblQuestionAsImage.setIcon(image);
//            lblQuestionAsImage.setText("");
            System.out.println("question.getQuestionImagePath()"+question.getQuestionImagePath());
            loadImage(question.getQuestionImagePath(), lblQuestionAsImage);                
        }
        if (!question.isIsOptionAsImage()) {
            showOptions();
            setLableText(lblOptionA, question.getOptionA());
            setLableText(lblOptionB, question.getOptionB());
            setLableText(lblOptionC, question.getOptionC());
            setLableText(lblOptionD, question.getOptionD());
        } else {
            hideOptions();
            System.out.println("question.getQuestionImagePath()"+question.getOptionImagePath());
            loadImage(question.getOptionImagePath(), lblOptionAsImage);
//            ImageIcon image = new EmployeeShow().RetriveImage(question.getOptionImagePath());
//            lblOptionAsImage.setIcon(image);
//            lblOptionAsImage.setText("");
        }

        String userAnswer = question.getUserAnswer();
        if (userAnswer.equals("UnAttempted")) {
            unlockSelection();
        } else {
            if (userAnswer.equals("A")) {
                rdoOptionA.setSelected(true);
            } else if (userAnswer.equals("B")) {
                rdoOptionB.setSelected(true);
            } else if (userAnswer.equals("C")) {
                rdoOptionC.setSelected(true);
            }
            if (userAnswer.equals("D")) {
                rdoOptionD.setSelected(true);
            }
//            lockSelection();
        }
        lblQuestionNo.setText("Q. " + index);
        
    }

    public void lockSelection() {
        rdoOptionA.setEnabled(false);
        rdoOptionB.setEnabled(false);
        rdoOptionC.setEnabled(false);
        rdoOptionD.setEnabled(false);
    }

    public void unlockSelection() {
        buttonGroup1.clearSelection();
        rdoOptionA.setEnabled(true);
        rdoOptionB.setEnabled(true);
        rdoOptionC.setEnabled(true);
        rdoOptionD.setEnabled(true);
    }

//    Image bg = new ImageIcon(getClass().getResource("/ui/images/Background.jpg")).getImage();
//    
//    
//    @Override
//    public void paintComponent(Graphics g) {
//        g.drawImage(bg, 0, 0, getWidth(), getHeight(), this);
//    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        lblQuestionNo = new javax.swing.JLabel();
        lblQuestion = new javax.swing.JLabel();
        lblQuestionAsImage = new javax.swing.JLabel();
        lblQuestionNo6 = new javax.swing.JLabel();
        lblOptionA = new javax.swing.JLabel();
        lblOptionAsImage = new javax.swing.JLabel();
        lblB = new javax.swing.JLabel();
        lblOptionB = new javax.swing.JLabel();
        lblC = new javax.swing.JLabel();
        lblOptionC = new javax.swing.JLabel();
        lblD = new javax.swing.JLabel();
        lblOptionD = new javax.swing.JLabel();
        lblA = new javax.swing.JLabel();
        rdoOptionD = new javax.swing.JRadioButton();
        lblQuestionNo5 = new javax.swing.JLabel();
        rdoOptionB = new javax.swing.JRadioButton();
        rdoOptionC = new javax.swing.JRadioButton();
        rdoOptionA = new javax.swing.JRadioButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setAutoscrolls(true);

        lblQuestionNo.setBackground(new java.awt.Color(255, 255, 255));
        lblQuestionNo.setFont(new java.awt.Font("Segoe UI Semibold", 1, 16)); // NOI18N
        lblQuestionNo.setText("Q. 1");
        lblQuestionNo.setName("lblQuestionNo"); // NOI18N

        lblQuestion.setBackground(new java.awt.Color(255, 255, 255));
        lblQuestion.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblQuestion.setText("Question"); // NOI18N
        lblQuestion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQuestion.setName("lblQuestion"); // NOI18N

        lblQuestionAsImage.setBackground(new java.awt.Color(255, 255, 255));
        lblQuestionAsImage.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblQuestionAsImage.setText("Question Image");
        lblQuestionAsImage.setToolTipText("Click Here For Zoom Image.");
        lblQuestionAsImage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQuestionAsImage.setName("lblQuestionAsImage"); // NOI18N
        lblQuestionAsImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQuestionAsImageMouseClicked(evt);
            }
        });

        lblQuestionNo6.setBackground(new java.awt.Color(255, 255, 255));
        lblQuestionNo6.setFont(new java.awt.Font("Segoe UI Semibold", 1, 16)); // NOI18N
        lblQuestionNo6.setText("Options :");
        lblQuestionNo6.setName("lblQuestionNo6"); // NOI18N

        lblOptionA.setBackground(new java.awt.Color(255, 255, 255));
        lblOptionA.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblOptionA.setText("jLabel7");
        lblOptionA.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionA.setName("lblOptionA"); // NOI18N

        lblOptionAsImage.setBackground(new java.awt.Color(255, 255, 255));
        lblOptionAsImage.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblOptionAsImage.setText("Option Image");
        lblOptionAsImage.setToolTipText("Click Here For Zoom Image.");
        lblOptionAsImage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionAsImage.setName("lblOptionAsImage"); // NOI18N
        lblOptionAsImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblOptionAsImageMouseClicked(evt);
            }
        });

        lblB.setBackground(new java.awt.Color(255, 255, 255));
        lblB.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblB.setText("B :");
        lblB.setName("lblB"); // NOI18N

        lblOptionB.setBackground(new java.awt.Color(255, 255, 255));
        lblOptionB.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblOptionB.setText("jLabel7");
        lblOptionB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionB.setName("lblOptionB"); // NOI18N

        lblC.setBackground(new java.awt.Color(255, 255, 255));
        lblC.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblC.setText("C :");
        lblC.setName("lblC"); // NOI18N

        lblOptionC.setBackground(new java.awt.Color(255, 255, 255));
        lblOptionC.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblOptionC.setText("jLabel7");
        lblOptionC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionC.setName("lblOptionC"); // NOI18N

        lblD.setBackground(new java.awt.Color(255, 255, 255));
        lblD.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblD.setText("D :");
        lblD.setName("lblD"); // NOI18N

        lblOptionD.setBackground(new java.awt.Color(255, 255, 255));
        lblOptionD.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblOptionD.setText("jLabel7");
        lblOptionD.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionD.setName("lblOptionD"); // NOI18N

        lblA.setBackground(new java.awt.Color(255, 255, 255));
        lblA.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblA.setText("A :");
        lblA.setName("lblA"); // NOI18N

        rdoOptionD.setBackground(new java.awt.Color(255, 255, 255));
        rdoOptionD.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        rdoOptionD.setText("D");
        rdoOptionD.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionD.setName("rdoOptionD"); // NOI18N
        rdoOptionD.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionDItemStateChanged(evt);
            }
        });

        lblQuestionNo5.setBackground(new java.awt.Color(255, 255, 255));
        lblQuestionNo5.setFont(new java.awt.Font("Segoe UI Semibold", 1, 16)); // NOI18N
        lblQuestionNo5.setText("Select Answer:");
        lblQuestionNo5.setName("lblQuestionNo5"); // NOI18N

        rdoOptionB.setBackground(new java.awt.Color(255, 255, 255));
        rdoOptionB.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        rdoOptionB.setText("B");
        rdoOptionB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionB.setName("rdoOptionB"); // NOI18N
        rdoOptionB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionBItemStateChanged(evt);
            }
        });

        rdoOptionC.setBackground(new java.awt.Color(255, 255, 255));
        rdoOptionC.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        rdoOptionC.setText("C");
        rdoOptionC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionC.setName("rdoOptionC"); // NOI18N
        rdoOptionC.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionCItemStateChanged(evt);
            }
        });

        rdoOptionA.setBackground(new java.awt.Color(255, 255, 255));
        rdoOptionA.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        rdoOptionA.setText("A");
        rdoOptionA.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoOptionA.setName("rdoOptionA"); // NOI18N
        rdoOptionA.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionAItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblQuestionNo5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rdoOptionA)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdoOptionB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdoOptionC)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdoOptionD))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblQuestionNo)
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblQuestionAsImage)
                            .addComponent(lblQuestion)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblQuestionNo6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblB)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionB))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionA))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblC)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionC))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblD)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionD))
                            .addComponent(lblOptionAsImage))))
                .addContainerGap(471, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo)
                    .addComponent(lblQuestion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblQuestionAsImage)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo6)
                    .addComponent(lblA)
                    .addComponent(lblOptionA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB)
                    .addComponent(lblOptionB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblC)
                    .addComponent(lblOptionC))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblD)
                    .addComponent(lblOptionD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblOptionAsImage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo5)
                    .addComponent(rdoOptionA)
                    .addComponent(rdoOptionB)
                    .addComponent(rdoOptionC)
                    .addComponent(rdoOptionD))
                .addContainerGap(165, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

private void rdoOptionAItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionAItemStateChanged
}//GEN-LAST:event_rdoOptionAItemStateChanged

private void rdoOptionBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionBItemStateChanged
}//GEN-LAST:event_rdoOptionBItemStateChanged

private void rdoOptionCItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionCItemStateChanged
}//GEN-LAST:event_rdoOptionCItemStateChanged

private void rdoOptionDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionDItemStateChanged
}//GEN-LAST:event_rdoOptionDItemStateChanged

    private void lblQuestionAsImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQuestionAsImageMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                new ZoomImage(qImagePath).setVisible(true);
            }
        }

    }//GEN-LAST:event_lblQuestionAsImageMouseClicked

    private void lblOptionAsImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOptionAsImageMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                new ZoomImage(oImagePath).setVisible(true);

                System.out.println(oImagePath);
            }
        }
    }//GEN-LAST:event_lblOptionAsImageMouseClicked
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel lblA;
    private javax.swing.JLabel lblB;
    private javax.swing.JLabel lblC;
    private javax.swing.JLabel lblD;
    private javax.swing.JLabel lblOptionA;
    private javax.swing.JLabel lblOptionAsImage;
    private javax.swing.JLabel lblOptionB;
    private javax.swing.JLabel lblOptionC;
    private javax.swing.JLabel lblOptionD;
    private javax.swing.JLabel lblQuestion;
    private javax.swing.JLabel lblQuestionAsImage;
    private javax.swing.JLabel lblQuestionNo;
    private javax.swing.JLabel lblQuestionNo5;
    private javax.swing.JLabel lblQuestionNo6;
    private javax.swing.JRadioButton rdoOptionA;
    private javax.swing.JRadioButton rdoOptionB;
    private javax.swing.JRadioButton rdoOptionC;
    private javax.swing.JRadioButton rdoOptionD;
    // End of variables declaration//GEN-END:variables
}
