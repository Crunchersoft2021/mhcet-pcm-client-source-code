package JEE.practice;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import server.DBConnection1;
import server.Server;

public class SavePractice {
    
    public void savePracticeBean(PracticeBean practiceBean,int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try
        {  
            int testId=0;
            Statement s=conn.createStatement();
            ResultSet rs = s.executeQuery("Select max(id) from Saved_Test_Info");
                if(rs.next())
                {
                        testId=rs.getInt(1);
                }
            testId++;
            String sql="insert into Saved_Test_Info values(?,?,?,?,?)";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, testId);
            ps.setInt(2, rollNo);
            ps.setInt(3, 2);//TestBean = 1
            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
            ObjectOutputStream objOutStream = new ObjectOutputStream(byteOutStream);
            objOutStream.writeObject(practiceBean);
            objOutStream.flush();
            objOutStream.close();
            byteOutStream.close();
            ps.setBytes(5, byteOutStream.toByteArray());
            int i=ps.executeUpdate();
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public ArrayList<Integer> getSavedTestId(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<Integer> ids=new ArrayList<Integer>();
        try{
            String sql="select * from Saved_Test_Info where testType=2 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                int s= rs.getInt(1);
                ids.add(s);
            }
            return ids;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return ids;
    }
    
    public ArrayList<PracticeBean> retrievePracticeBean(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<PracticeBean> practiceBeans=new ArrayList<PracticeBean>();
        try
        {  
            String sql="select * from Saved_Test_Info where testType=2 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                PracticeBean p = new PracticeBean();
                byte[] data = rs.getBytes(5) ;
                ByteArrayInputStream byteInStream = new ByteArrayInputStream(data);
                ObjectInputStream objInStream = new ObjectInputStream(byteInStream);
                Object obj = objInStream.readObject();
                objInStream.close();
                byteInStream.close();
                if (obj instanceof PracticeBean) {
                    p = (PracticeBean)obj;
                }
                practiceBeans.add(p);
            }
            return practiceBeans;
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return null;
    }
    
    public void delPractice(int practiceId)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try
        {
            String sql="delete from Saved_Test_Info where id=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, practiceId);
            int i=ps.executeUpdate();
            if(i>0)
                JOptionPane.showMessageDialog(null, "Deleted Successfully.");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public ArrayList<String> getDate(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<String> savedTestDate=new ArrayList<String>();
        try{
            String sql="select * from Saved_Test_Info where testType=2 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                String s= rs.getString(4);
                savedTestDate.add(s);
            }
            return savedTestDate;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return savedTestDate;
    }

    public Integer isTestPresent(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try{
            String sql="select * from Saved_Test_Info where testType=2 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            if(rs.next())
            {
                return 1;
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return 0;
    }  
}