/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JEE.test;

public class TestResultBean {
    
    private int testId;
    private int totalPhysics,totalChemistry,totalMaths;
    private int obtainedPhysics,obtainedChemistry,obtainedMaths;

    public TestResultBean() {
    }

    public TestResultBean(int testId,int totalPhysics, int totalChemistry, int totalMaths, int obtainedPhysics, int obtainedChemistry, int obtainedMaths) {
        this.testId = testId;
        this.totalPhysics = totalPhysics;
        this.totalChemistry = totalChemistry;
        this.totalMaths = totalMaths;
        this.obtainedPhysics = obtainedPhysics;
        this.obtainedChemistry = obtainedChemistry;
        this.obtainedMaths = obtainedMaths;
    }

    public int getTestId() {
        return testId;
    }

    public void setObtainedMaths(int obtainedMaths) {
        this.obtainedMaths = obtainedMaths;
    }

    public void setTotalMaths(int totalMaths) {
        this.totalMaths = totalMaths;
    }

    public int getObtainedMaths() {
        return obtainedMaths;
    }

    public int getTotalMaths() {
        return totalMaths;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }
    
    public int getobtainedMaths() {
        return obtainedMaths;
    }

    public void setobtainedMaths(int obtainedMaths) {
        this.obtainedMaths = obtainedMaths;
    }

    public int getObtainedChemistry() {
        return obtainedChemistry;
    }

    public void setObtainedChemistry(int obtainedChemistry) {
        this.obtainedChemistry = obtainedChemistry;
    }

    public int getObtainedPhysics() {
        return obtainedPhysics;
    }

    public void setObtainedPhysics(int obtainedPhysics) {
        this.obtainedPhysics = obtainedPhysics;
    }

    public int gettotalMaths() {
        return totalMaths;
    }

    public void settotalMaths(int totalMaths) {
        this.totalMaths = totalMaths;
    }

    public int getTotalChemistry() {
        return totalChemistry;
    }

    public void setTotalChemistry(int totalChemistry) {
        this.totalChemistry = totalChemistry;
    }

    public int getTotalPhysics() {
        return totalPhysics;
    }

    public void setTotalPhysics(int totalPhysics) {
        this.totalPhysics = totalPhysics;
    }
}