package NEET.graphs;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.util.ArrayList;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import NEET.test.DBConnection;
import NEET.test.TestResultForGraph;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;

public class BarChartDemo1 extends JFrame {
    private static final long serialVersionUID = 1L;
    int rollNo;
    private static ArrayList<NEET.test.TestResultForGraph> testResultForGraphs=new ArrayList<TestResultForGraph>();
    {
        ChartFactory.setChartTheme(new StandardChartTheme("JFree/Shadow",
                true));
    }
    
    public BarChartDemo1(String title,int rollNo) {
        super(title);
        this.rollNo=rollNo;        
        setIconImage(new ImageIcon(getClass().getResource("/ui/images/c.gif")).getImage());
        CategoryDataset dataset = createDataset();
        JFreeChart chart = createChart(dataset);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setFillZoomRectangle(true);
        chartPanel.setMouseWheelEnabled(true);
        chartPanel.setPreferredSize(new Dimension(700, 470));
        setContentPane(chartPanel);
        setLocationRelativeTo(null);
        JScrollPane jcJScrollPane=new JScrollPane();
        this.add(jcJScrollPane);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    private CategoryDataset createDataset() {
        NEET.test.DBConnection db=new DBConnection();
        testResultForGraphs=db.getResultObjects(rollNo);
        int id=db.getMaxTestResId();
        String category[]=new String[id];
        for(int i=0;i<id;i++){
            int j=i+1;
            category[i]="Test"+j;
        }
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();       
            TestResultForGraph testResultForGraph=new TestResultForGraph();
            for(int i=0;i<testResultForGraphs.size();i++){
                testResultForGraph=testResultForGraphs.get(i);
                int mark=testResultForGraph.getCorrect_Questions();
                int sub=testResultForGraph.getSubject_Id();
                int testid=testResultForGraph.getTest_Id();
                testid--;
                String series = "";
                if(sub==1)
                    series="Physics";
                else if(sub==2)
                    series="Chemistry";
                else if(sub==3)
                    series="Biology";
                dataset.addValue(mark, series, category[testid]);
            }
        return dataset;
    }

    private static JFreeChart createChart(CategoryDataset dataset) {
        JFreeChart chart = ChartFactory.createBarChart(
            "Overall Analysis",       // chart title
            "Subjects",               // domain axis label
            "Marks",                  // range axis label
            dataset,                  // data
            PlotOrientation.VERTICAL, // orientation
            true,                     // include legend
            true,                     // tooltips?
            false                     // URLs?
        );
        chart.setBackgroundPaint(Color.white);
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        GradientPaint gp0 = new GradientPaint(0.0f, 0.0f, Color.blue,
                0.0f, 0.0f, new Color(0, 0, 64));
        GradientPaint gp1 = new GradientPaint(0.0f, 0.0f, Color.green,
                0.0f, 0.0f, new Color(0, 64, 0));
        GradientPaint gp2 = new GradientPaint(0.0f, 0.0f, Color.red,
                0.0f, 0.0f, new Color(64, 0, 0));
        renderer.setSeriesPaint(0, gp0);
        renderer.setSeriesPaint(1, gp1);
        renderer.setSeriesPaint(2, gp2);
        CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(
                        Math.PI / 6.0));
        return chart;
    }
    public static void main(String[] args) {
    }
}